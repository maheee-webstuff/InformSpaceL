
ISL = (function () {

  var $template = null;

  $(function () {
    $template = $("div.page").detach();

    //$("#content").draggable
  });

  function Page() {
    this.htmlPresentation = null;
    this.imagePresentation = null;
    this.markdownPresentation = "# New Page";

    this.$element = $template.clone();
    this.$element.addClass("page");

    this.$element.draggable({
      handle: ".header",
      stack: ".page"
      //zIndex: 10
    }).resizable({
      minWidth: 100,
      minHeight: 60
    });

    this.updateHtml = function () {
      this.htmlPresentation = marked(this.markdownPresentation);
      this.$element.children(".content").html(this.htmlPresentation);
    };
  };


  return {
    Page: Page
  };
})();


function addPage() {
  var page = new ISL.Page();

  page.updateHtml();

  $("div#content").append(page.$element);
};


$(function () {

});
